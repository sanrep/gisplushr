from os import fdopen, remove
from shutil import move
from tempfile import mkstemp

from django.contrib.staticfiles.storage import staticfiles_storage
from django.utils.translation import gettext_lazy as _

from osgeo.gdal import OpenEx, UseExceptions, VectorTranslate

from .common import check_or_fetch
from .models import CoordinateSystem, CoordinateSystemTransformation, GisFormat


# ----------------------------------------------------------------------
def correct_mif(mif_filename, coordinate_system):
    """Correct coordinate system in mif file.

    Correct coordinate system in mif file for custom coordinate systems.

    :param mif_filename: Full path of mif file which needs correction as
                         a string
    :param coordinate_system: CoordinateSystem object used for mif file

    :return: bool -- ``True`` if mif file is modified successfully,
             ``False`` if no changes were necessary (mif file is not
             changed if mif_definition of coordinate_system is empty)

    :raise ValueError: If coordinate_system is not instance of
                       CoordinateSystem
    :raise OSError: In case of FileExistsError, FileNotFoundError,
                    PermissionError or "disk full"

    Mif file is rewritten line by line.
    TODO: Consider using shutil.copyfileobj(fsrc, fdst[, length])
    """
    if not isinstance(coordinate_system, CoordinateSystem):
        raise ValueError(_('Coordinate system not defined!'))
    mif_proj_string = getattr(coordinate_system, 'mif_definition', '')
    if not mif_proj_string:
        # If mif_definirion is empty for coordinate_system,
        # correction is not needed
        return False

    # mkstemp() returns a tuple containing an OS-level handle to an open file
    # and the absolute pathname of that file
    fh, abs_path = mkstemp()
    try:
        with open(mif_filename) as mif_dat_orig:
            with fdopen(fh, 'w') as mif_dat_temp:
                for i, line in enumerate(mif_dat_orig):
                    # 4th line in mif file (i=3) should contain coordinate system information
                    if i == 3:
                        # Write correct definition of coordinate system
                        mif_dat_temp.write(mif_proj_string + '\n')
                        # Skip copying of coordinate system definition if it exists
                        if line[0:8] == 'CoordSys':
                            continue
                    mif_dat_temp.write(line)
            move(abs_path, mif_filename)
    except OSError:
        remove(abs_path)
        raise
    return True


# ----------------------------------------------------------------------
def gdal_transform(input_datasource, output_datasource=None,
                   input_format=None, output_format=None,
                   input_coordinate_system=None, output_coordinate_system=None,
                   transformation=None):
    """Transform GIS datasource using GDAL.

    If formats and coordinate systems are the same, output_datasource is
    a copy of input_datasource.

    :param input_datasource: name, or path of input datasource
    :param output_datasource: name, or path of output datasource
    :param input_format: tag or instance of GisFormat class
    :param output_format: tag or instance of GisFormat class. Defaults
                          to input_format if not defined.
    :param input_coordinate_system: tag or instance of CoordinateSystem
                                    class. Needs to be defined for
                                    transformation.
    :param output_coordinate_system: tag or instance of CoordinateSystem
                                     class. Needs to be defined for
                                     transformation, or if output format
                                     is MIF/MID.
    :param transformation: tag or instance of
                           CoordinateSystemTransformation class

    :raise ValueError: If input_datasource or output_datasource are not
                       defined, or if output_coordinate_system is not
                       defined and output_format is MIF/MID.
    :raise RuntimeError: If VectorTranslate returns error
    :raise OSError: In case disk is full during mif file correction
    :raise ObjectDoesNotExist: If tag of input parameters does not exist
    :raise MultipleObjectsReturned: If there are multiple objects with
                                    the same tag.
    """

    if not input_datasource:
        raise ValueError(_('Input datasource not defined!'))
    # TODO: If output_datasource is not defined, create name from input_datasource
    if not output_datasource:
        raise ValueError(_('Output datasource not defined!'))

    # TODO: If input_format is undefined, try to guess it
    input_format = check_or_fetch(
        GisFormat,
        input_format,
        'tag',
        GisFormat.objects.get(tag='shp'),
    )
    output_format = check_or_fetch(
        GisFormat,
        output_format,
        'tag',
        input_format,
    )
    input_coordinate_system = check_or_fetch(
        CoordinateSystem,
        input_coordinate_system,
        'tag',
    )
    output_coordinate_system = check_or_fetch(
        CoordinateSystem,
        output_coordinate_system,
        'tag',
        input_coordinate_system,
    )
    transformation = check_or_fetch(
        CoordinateSystemTransformation,
        transformation,
        'tag',
    )
    # TODO: Check input/output datasource extension against format

    vt_opts = {}  # VectorTranslateOptions

    if (input_coordinate_system is not None and
            output_coordinate_system is not None and
            input_coordinate_system != output_coordinate_system):
        if transformation is None:
            # If transformation is undefined, fetch first transformation defined
            # for coordination systems (official transformation are fetched first)
            transformation = CoordinateSystemTransformation.objects.filter(
                input_coordinate_systems=input_coordinate_system,
                output_coordinate_systems=output_coordinate_system).order_by(
                '-official_transformation').first()
        if transformation is None:
            # Transformation without special parameters
            vt_opts.update(
                srcSRS=input_coordinate_system.get_gdal_definition(),
                dstSRS=output_coordinate_system.get_gdal_definition(),
                reproject=True,
            )
        else:
            vt_opts.update(
                srcSRS=transformation.input_parameters.get_gdal_definition(),
                dstSRS=transformation.output_parameters.get_gdal_definition(),
                reproject=True,
            )
        # In case grid transformation is used, get grid file path
        grid_path = f'{staticfiles_storage.path("grids")}/'
        # base_dir = getattr(settings, 'BASE_DIR', None)
        # grid_path = f'{base_dir}/grids/' if base_dir else 'grids/'
        vt_opts['srcSRS'] = vt_opts['srcSRS'].replace('+nadgrids=', '+nadgrids=' + grid_path)
        vt_opts['dstSRS'] = vt_opts['dstSRS'].replace('+nadgrids=', '+nadgrids=' + grid_path)

    if output_format.tag == 'mif':
        vt_opts.update(
            datasetCreationOptions=['FORMAT=MIF'],
        )

    # By default the GDAL/OGR Python bindings do not raise exceptions when errors occur
    # Enable GDAL/OGR exceptions
    UseExceptions()

    print(output_format, vt_opts)
    input = OpenEx(input_datasource)
    output = VectorTranslate(destNameOrDestDS=output_datasource, srcDS=input,
                             format=output_format.gdal_name, **vt_opts)
    # Output file is written after dereferencing dataset object
    del output
    if output_format.tag == 'mif':
        correct_mif(output_datasource, output_coordinate_system)
