from django.apps import AppConfig


class GisplushrConfig(AppConfig):
    name = 'gisplushr'
