from django.db import models
from django.utils.translation import gettext_lazy as _

from djplus.models import ActivatableModel, ActivatableQuerySet


# ----------------------------------------------------------------------
class GisFormat(ActivatableModel):
    """GIS formats

       tag - tag or extension for GIS format
       gdal_name - GDAL name for GIS format
    """
    tag = models.CharField(
        max_length=7,
        verbose_name=_('tag'),
    )
    gdal_name = models.CharField(
        max_length=20,
        verbose_name=_('GDAL name'),
    )

    objects = ActivatableQuerySet.as_manager()

    class Meta:
        verbose_name = _('GIS format')
        verbose_name_plural = _('GIS formats')
        default_permissions = []

    def __str__(self):
        return self.tag


# ----------------------------------------------------------------------
class CoordinateSystem(ActivatableModel):
    """GIS coordinate systems."""
    tag = models.CharField(
        max_length=40,
        verbose_name=_('tag'),
    )
    name = models.CharField(
        max_length=60,
        verbose_name=_('name'),
    )
    description = models.TextField(
        blank=True,
        verbose_name=_('description'),
    )
    mif_definition = models.TextField(
        blank=True,
        verbose_name=_('MIF definition'),
    )
    proj4_definition = models.TextField(
        blank=True,
        verbose_name=_('PROJ4 definition'),
    )
    epsg = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_('EPSG'),
    )
    transformation = models.BooleanField(
        default=False,
        verbose_name=_('transformation'),
    )

    objects = ActivatableQuerySet.as_manager()

    class Meta:
        verbose_name = _('GIS coordinate system')
        verbose_name_plural = _('GIS coordinate systems')
        default_permissions = []

    def __str__(self):
        return self.tag

    def get_gdal_definition(self):
        if self.epsg is not None:
            return f'EPSG:{self.epsg}'
        return self.proj4_definition


# ----------------------------------------------------------------------
class CoordinateSystemTransformation(ActivatableModel):
    """Parameters for coordinate system transformation."""
    tag = models.CharField(
        max_length=40,
        verbose_name=_('tag'),
    )
    name = models.CharField(
        max_length=60,
        verbose_name=_('name'),
    )
    description = models.TextField(
        blank=True,
        verbose_name=_('description'),
    )
    input_parameters = models.ForeignKey(
        CoordinateSystem,
        on_delete=models.PROTECT,
        related_name='input_transformation',
        verbose_name=_('input parameters'),
    )
    output_parameters = models.ForeignKey(
        CoordinateSystem,
        on_delete=models.PROTECT,
        related_name='output_transformation',
        verbose_name=_('output parameters'),
    )
    input_coordinate_systems = models.ManyToManyField(
        CoordinateSystem,
        blank=True,
        related_name='input_transformations',
        verbose_name=_('input coordinate systems'),
    )
    output_coordinate_systems = models.ManyToManyField(
        CoordinateSystem,
        blank=True,
        related_name='output_transformations',
        verbose_name=_('output coordinate systems'),
    )
    official_transformation = models.BooleanField(
        default=False,
        verbose_name=_('official transformation'),
    )

    objects = ActivatableQuerySet.as_manager()

    class Meta:
        verbose_name = _('coordinate system transformation')
        verbose_name_plural = _('coordinate system transformations')
        default_permissions = []

    def __str__(self):
        return self.tag
