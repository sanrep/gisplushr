from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist

import pytest

from gisplushr.common import check_or_fetch


@pytest.mark.django_db
def test_check_or_fetch_instance():
    usermodel = get_user_model()
    new_user = usermodel.objects.create()
    assert check_or_fetch(usermodel, new_user) == new_user


@pytest.mark.django_db
def test_check_or_fetch_id():
    usermodel = get_user_model()
    new_user = usermodel.objects.create()
    assert check_or_fetch(usermodel, new_user.id) == new_user


@pytest.mark.django_db
def test_check_or_fetch_username():
    usermodel = get_user_model()
    new_user = usermodel.objects.create(username='admin')
    assert check_or_fetch(usermodel, new_user.username, attr='username') == new_user


@pytest.mark.django_db
def test_check_or_fetch_var_is_none():
    usermodel = get_user_model()
    assert check_or_fetch(usermodel, None) is None


@pytest.mark.django_db
def test_check_or_fetch_var_is_none_return_0():
    usermodel = get_user_model()
    assert check_or_fetch(usermodel, None, default_if_none=0) == 0


@pytest.mark.django_db
def test_check_or_fetch_no_object():
    usermodel = get_user_model()
    usermodel.objects.create(username='admin')
    with pytest.raises(ObjectDoesNotExist):
        check_or_fetch(usermodel, 'doesnot exist', 'username')
