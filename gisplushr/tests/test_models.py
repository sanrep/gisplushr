import pytest

from gisplushr.models import CoordinateSystem

from .factories import (
    CoordinateSystemFactory,
    CoordinateSystemTransformationFactory,
    GisFormatFactory,
)

pytestmark = pytest.mark.django_db


def test_gisformat_string_representation():
    obj = GisFormatFactory()
    assert obj.__str__() == obj.tag
    assert str(obj) == obj.tag


def test_coordinatesystem_string_representation():
    obj = CoordinateSystemFactory()
    assert obj.__str__() == obj.tag
    assert str(obj) == obj.tag


def test_coordinatesystemtransformation_string_representation():
    obj = CoordinateSystemTransformationFactory()
    assert obj.__str__() == obj.tag
    assert str(obj) == obj.tag


def test_coordinatesystem_get_gdal_definition():
    tag = 'htrs96tm'
    obj = CoordinateSystem.objects.get(tag=tag)
    assert obj.get_gdal_definition() == f'EPSG:{obj.epsg}'

    tag = 'hcr gk5'
    obj = CoordinateSystem.objects.get(tag=tag)
    assert obj.get_gdal_definition() == obj.proj4_definition
