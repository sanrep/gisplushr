import factory
import factory.fuzzy

from gisplushr.models import (
    CoordinateSystem,
    CoordinateSystemTransformation,
    GisFormat,
)


class GisFormatFactory(factory.django.DjangoModelFactory):
    tag = factory.fuzzy.FuzzyText()
    gdal_name = factory.fuzzy.FuzzyText()

    class Meta:
        model = GisFormat


class CoordinateSystemFactory(factory.django.DjangoModelFactory):
    tag = factory.fuzzy.FuzzyText()
    name = factory.fuzzy.FuzzyText()

    class Meta:
        model = CoordinateSystem


class CoordinateSystemTransformationFactory(factory.django.DjangoModelFactory):
    tag = factory.fuzzy.FuzzyText()
    name = factory.fuzzy.FuzzyText()
    input_parameters = factory.SubFactory(CoordinateSystemFactory)
    output_parameters = factory.SubFactory(CoordinateSystemFactory)

    class Meta:
        model = CoordinateSystemTransformation
