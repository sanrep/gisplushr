from os import path

from django.core.exceptions import ObjectDoesNotExist

import pytest
from osgeo import ogr, osr

from gisplushr.models import (
    CoordinateSystem,
    CoordinateSystemTransformation,
    GisFormat,
)
from gisplushr.utils import correct_mif, gdal_transform

from .factories import CoordinateSystemFactory


# ----------------------------------------------------------------------
def create_shape_file(filename, epsg=3765):
    """Create test shapefile with id and a point."""
    driver = ogr.GetDriverByName("ESRI Shapefile")
    shp_file = driver.CreateDataSource(filename)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg)
    layer = shp_file.CreateLayer('testing', srs, ogr.wkbPoint)
    layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger))
    feature = ogr.Feature(layer.GetLayerDefn())
    feature.SetField('id', 1)
    feature.SetGeometry(ogr.CreateGeometryFromWkt('POINT(500000 4900000)'))
    layer.CreateFeature(feature)
    feature = None
    shp_file = None


# ----------------------------------------------------------------------
# correct_mif
# ----------------------------------------------------------------------
def test_correct_mif_wrong_coordinate_system():
    with pytest.raises(ValueError):
        correct_mif('', None)
    with pytest.raises(ValueError):
        correct_mif('', 'xyz')


@pytest.mark.django_db
def test_correct_mif_coordinate_system_no_mif_definition():
    filenames = [
        '',
        'path/filename.mif',
    ]
    coordinate_system = CoordinateSystemFactory()
    for filename in filenames:
        assert correct_mif(filename, coordinate_system) is False


@pytest.mark.django_db
def test_correct_mif_no_mif_file(tempdir):
    coordinate_system = CoordinateSystemFactory(
        mif_definition='CoordSys Earth Projection 8, 104')
    mif_filename = path.join(tempdir, 'filename.mif')
    with pytest.raises(OSError):
        correct_mif(mif_filename, coordinate_system)


@pytest.mark.django_db
def test_correct_mif_no_coordianate_system(tempdir):
    coordinate_system = CoordinateSystemFactory(
        mif_definition='CoordSys Earth Projection 8, 104')
    mif_filename = path.join(tempdir, 'filename.mif')
    with open(mif_filename, 'w') as f:
        for i in range(5):
            f.write(f'line {i+1}\n')
    assert correct_mif(mif_filename, coordinate_system) is True

    # check 4th line in mif file
    fourth_line = ''
    with open(mif_filename, 'r') as f:
        for i, line in enumerate(f):
            if i == 3:
                fourth_line = line
    assert fourth_line == f'{coordinate_system.mif_definition}\n'


@pytest.mark.django_db
def test_correct_mif(tempdir):
    coordinate_system = CoordinateSystemFactory(
        mif_definition='CoordSys Earth Projection 8, 104')
    mif_filename = path.join(tempdir, 'msp.mif')
    with open(mif_filename, 'w') as f:
        for i in range(6):
            if i == 3:
                f.write('CoordSys Earth Projection 5')
            else:
                f.write(f'line {i+1}\n')
    assert correct_mif(mif_filename, coordinate_system) is True

    # check 4th line in mif file
    fourth_line = ''
    with open(mif_filename, 'r') as f:
        for i, line in enumerate(f):
            if i == 3:
                fourth_line = line
    assert fourth_line == f'{coordinate_system.mif_definition}\n'


# ----------------------------------------------------------------------
# transform using GDAL
# ----------------------------------------------------------------------
@pytest.mark.django_db
def test_gdal_transform_all_parameters_as_objects(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # all parameters defined as objects
    parameters = {
        'output_datasource': output_datasource,
        'input_format': GisFormat.objects.get(tag='shp'),
        'output_format': GisFormat.objects.get(tag='mif'),
        'input_coordinate_system': CoordinateSystem.objects.get(tag='htrs96tm'),
        'output_coordinate_system': CoordinateSystem.objects.get(tag='gk5'),
        'transformation': CoordinateSystemTransformation.objects.get(
            tag='htrs >> gk5 (grid)'),
    }
    create_shape_file(input_datasource)
    gdal_transform(input_datasource, **parameters)
    assert path.isfile(output_datasource) is True


@pytest.mark.django_db
def test_gdal_transform_all_parameters_as_tags(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # all parameters defined as tags
    parameters = {
        'output_datasource': output_datasource,
        'input_format': 'shp',
        'output_format': 'mif',
        'input_coordinate_system': 'htrs96tm',
        'output_coordinate_system': 'gk5',
        'transformation': 'htrs >> gk5 (grid)',
    }
    create_shape_file(input_datasource)
    gdal_transform(input_datasource, **parameters)
    assert path.isfile(output_datasource) is True


@pytest.mark.django_db
def test_gdal_transform_no_transformation_defined(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # no transformation, but defined
    parameters = {
        'output_datasource': output_datasource,
        'input_format': 'shp',
        'output_format': 'mif',
        'input_coordinate_system': 'htrs96tm',
        'output_coordinate_system': 'gk5',
    }
    create_shape_file(input_datasource)
    gdal_transform(input_datasource, **parameters)
    assert path.isfile(output_datasource) is True


@pytest.mark.django_db
def test_gdal_transform_no_transformation_undefined(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # no transformation, and undefined
    parameters = {
        'output_datasource': output_datasource,
        'input_format': 'shp',
        'output_format': 'mif',
        'input_coordinate_system': 'gk6',
        'output_coordinate_system': 'gk5',
    }
    create_shape_file(input_datasource)
    gdal_transform(input_datasource, **parameters)
    assert path.isfile(output_datasource) is True


@pytest.mark.django_db
def test_gdal_transform_no_output_coordinate_system(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # no output coordinate system
    parameters = {
        'output_datasource': output_datasource,
        'input_format': 'shp',
        'output_format': 'mif',
        'input_coordinate_system': 'htrs96tm',
    }
    create_shape_file(input_datasource)
    gdal_transform(input_datasource, **parameters)
    assert path.isfile(output_datasource) is True


@pytest.mark.django_db
def test_gdal_transform_no_output_format(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.shp')
    # no output format
    parameters = {
        'output_datasource': output_datasource,
        'input_format': 'shp',
        'input_coordinate_system': 'htrs96tm',
        'output_coordinate_system': 'gk5',
    }
    create_shape_file(input_datasource)
    gdal_transform(input_datasource, **parameters)
    assert path.isfile(output_datasource) is True


@pytest.mark.django_db
def test_gdal_transform_no_input_datasource(tempdir):
    with pytest.raises(ValueError):
        gdal_transform('')


@pytest.mark.django_db
def test_gdal_transform_no_output_datasource(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    with pytest.raises(ValueError):
        gdal_transform(input_datasource)


@pytest.mark.django_db
def test_gdal_transform_wrong_input_format(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # tag of input format doesn't exist
    parameters = {
        'output_datasource': output_datasource,
        'input_format': 'does not exist',
    }
    create_shape_file(input_datasource)
    with pytest.raises(ObjectDoesNotExist):
        gdal_transform(input_datasource, **parameters)


@pytest.mark.django_db
def test_gdal_transform_wrong_output_format(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # tag of output format doesn't exist
    parameters = {
        'output_datasource': output_datasource,
        'output_format': 'does not exist',
    }
    create_shape_file(input_datasource)
    with pytest.raises(ObjectDoesNotExist):
        gdal_transform(input_datasource, **parameters)


@pytest.mark.django_db
def test_gdal_transform_wrong_input_coordinate_system(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # tag of input coordinate system doesn't exist
    parameters = {
        'output_datasource': output_datasource,
        'input_coordinate_system': 'does not exist',
    }
    create_shape_file(input_datasource)
    with pytest.raises(ObjectDoesNotExist):
        gdal_transform(input_datasource, **parameters)


@pytest.mark.django_db
def test_gdal_transform_wrong_output_coordinate_system(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # tag of output coordinate system doesn't exist
    parameters = {
        'output_datasource': output_datasource,
        'output_coordinate_system': 'does not exist',
    }
    create_shape_file(input_datasource)
    with pytest.raises(ObjectDoesNotExist):
        gdal_transform(input_datasource, **parameters)


@pytest.mark.django_db
def test_gdal_transform_wrong_transformation(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # tag of transformation doesn't exist
    parameters = {
        'output_datasource': output_datasource,
        'transformation': 'does not exist',
    }
    create_shape_file(input_datasource)
    with pytest.raises(ObjectDoesNotExist):
        gdal_transform(input_datasource, **parameters)


@pytest.mark.django_db
def test_gdal_transform_no_output_coordinate_system_mif(tempdir):
    input_datasource = path.join(tempdir, 'input.shp')
    output_datasource = path.join(tempdir, 'output.mif')
    # undefined output coordinate system and MIF/MID format
    parameters = {
        'output_datasource': output_datasource,
        'output_format': 'mif',
    }
    create_shape_file(input_datasource)
    with pytest.raises(ValueError):
        gdal_transform(input_datasource, **parameters)
