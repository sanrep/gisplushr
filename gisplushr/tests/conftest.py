from shutil import rmtree
from tempfile import mkdtemp

import pytest


@pytest.fixture
def tempdir():
    directory = mkdtemp()
    yield directory
    rmtree(directory)
