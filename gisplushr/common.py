def check_or_fetch(model, var, attr='id', default_if_none=None):
    """Check instance type or fetch instance based on attribute value.

    Check that var is instance of a model class, or if not return
    instance with value ``var`` in field ``attr``.

    :param model: Model class
    :param var: Model instance or attribute value.
    :param attr: Model field, defaults to 'id'.
    :param default_if_none: Return this value if var is None, defaults
                            to None.
    :return: Instance of a model class
    :raise ObjectDoesNotExist: Field attr with value var does not exist.
    :raise MultipleObjectsReturned: There are several instances with
                                    value var in field attr.

    """
    if var is None:
        return default_if_none
    if isinstance(var, model):
        return var
    kwargs = {
        attr: var,
    }
    return model.objects.get(**kwargs)
