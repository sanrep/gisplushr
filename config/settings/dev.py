"""Development settings"""

from .base import *  # noqa: F403, F401
from .base import BASE_DIR, INSTALLED_APPS, MIDDLEWARE, TEMPLATES

DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(BASE_DIR.joinpath('db.sqlite3')),
    }
}

INSTALLED_APPS += [
    'rosetta',
]

# django-debug-toolbar
if DEBUG:
    INSTALLED_APPS += [
        'debug_toolbar',
    ]

    MIDDLEWARE += [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

    INTERNAL_IPS = [
        '127.0.0.1',
    ]

# Rosetta settings
ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = False
ROSETTA_MESSAGES_PER_PAGE = 50
ROSETTA_SHOW_AT_ADMIN_PANEL = True
ROSETTA_STORAGE_CLASS = 'rosetta.storage.SessionRosettaStorage'
