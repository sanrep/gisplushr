# Testing dependencies

pytest>=5.4.2
pytest-django>=3.9.0
coverage>=5.1
django-coverage-plugin>=1.8.0
pytest-cov>=2.8.1
factory-boy>=2.12.0
