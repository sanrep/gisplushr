# For more information, please refer to https://aka.ms/vscode-docker-python
FROM sanrep/python-gdal-hr:3.7-3.0

EXPOSE 8000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

WORKDIR /workspaces/gisplushr

# Install pip requirements
# ADD requirements.txt .
# ADD requirements/ .
# RUN python -m pip install -r requirements.txt

COPY requirements /workspaces/gisplushr/requirements
RUN pip install --upgrade pip
RUN pip install -r requirements/dev.txt


# WORKDIR /app
COPY . /workspaces/gisplushr/

# Switching to a non-root user, please refer to https://aka.ms/vscode-docker-python-user-rights
# RUN useradd appuser && chown -R appuser /app
# USER appuser

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
# File wsgi.py was not found in subfolder:gisplushr. Please enter the Python path to wsgi file.
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "config.wsgi"]
