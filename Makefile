.PHONY: help clean clean-build clean-pyc clean-test lint test docs html package
.DEFAULT_GOAL := help

help:
	@grep '^[a-zA-Z]' $(MAKEFILE_LIST) | sort | awk -F ':.*?## ' 'NF==2 {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'

clean: clean-build clean-pyc  # remove build and Python artifacts

clean-build:  # remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:  # remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:  # remove test and coverage artifacts
	rm -fr .cache/
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/

lint:  # check style with flake8 and isort
	flake8 .
	isort --check-only --diff --recursive .
	doc8 docs

test:  # run tests and coverage
	pytest --cov-report html

tox:  # run tests on every Python version with tox
	tox

docs:  # generate all predefined documentation
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(MAKE) -C docs singlehtml
	# $(MAKE) -C docs latexpdf

html:  # generate Sphinx HTML documentation
	$(MAKE) -C docs clean
	$(MAKE) -C docs html

package: clean  # package a release
	python setup.py sdist
	python setup.py bdist_wheel
